<?php

use App\Broadcasting\LeaveChannel;
use App\Broadcasting\UserChannel;
use Illuminate\Support\Facades\Broadcast;
use Modules\MobileApplication\Broadcasting\TeacherChannel;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('reset-password-confirmed.{email}', function (string $token, string $email) {
    return true;
});

Broadcast::channel('email-verification-confirmed.{email}', function (string $email) {
    return true;
});

Broadcast::channel('attendance-updated', UserChannel::class);
Broadcast::channel('leave-updated', UserChannel::class);
Broadcast::channel('leave-deleted', UserChannel::class);