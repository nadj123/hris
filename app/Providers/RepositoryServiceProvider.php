<?php

namespace App\Providers;

use App\Repositories\BaseRepository;
use App\Repositories\HolidayRepository;
use App\Repositories\Interfaces\IBaseRepository;
use App\Repositories\Interfaces\IHolidayRepository;
use App\Repositories\Interfaces\IUserAttendanceRepository;
use App\Repositories\Interfaces\IUserInformationRepository;
use App\Repositories\Interfaces\IUserLeaveRepository;
use App\Repositories\Interfaces\IUserRepository;
use App\Repositories\UserAttendanceRepository;
use App\Repositories\UserInformationRepository;
use App\Repositories\UserLeaveRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IBaseRepository::class, BaseRepository::class);
        $this->app->bind(IUserRepository::class, UserRepository::class);
        $this->app->bind(IUserInformationRepository::class, UserInformationRepository::class);
        $this->app->bind(IUserAttendanceRepository::class, UserAttendanceRepository::class);
        $this->app->bind(IUserLeaveRepository::class, UserLeaveRepository::class);
        $this->app->bind(IHolidayRepository::class, HolidayRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
