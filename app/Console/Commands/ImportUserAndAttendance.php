<?php

namespace App\Console\Commands;

use App\Constants\Locations;
use App\Libraries\ZKTecoLib;
use App\Services\UserService;
use App\Services\UserAttendanceService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class ImportUserAndAttendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:user-and-attendance {--startDate= : The start date for importing}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importing data of user and its attendance to save it to the database';

    public $userService;
    public $userAttendanceService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        UserService $userService,
        UserAttendanceService $userAttendanceService
    )
    {
        parent::__construct();
        set_time_limit(0);
        $this->userService = $userService;
        $this->userAttendanceService = $userAttendanceService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $zktecoLib = new ZKTecoLib();
        $users = $zktecoLib->getUsers();
        $attendances = $zktecoLib->getAttendances();
        $filterAttendances = $this->filterAttendances($attendances);

        foreach ($users as $key => $user) {
            $userId = (int)$user['userid'];
            $this->saveUser($userId, $user);
            $timeInsAndTimeOuts = $this->getTimeInsAndTimeOuts($userId, $filterAttendances);
            $this->saveUserAttendance($userId, $timeInsAndTimeOuts);
        }

        $this->info('User and attendance imported successfully!');
    }

    /**
     * Get logs per day
     */
    protected function filterAttendances(array $attendances) : array
    {
        $filterAttendances = $filterAttendances[] = Collection::make($attendances)->filter(
            function ($item) {
                $timestamp = $this->option('startDate') ?? Carbon::now()->format('Y-m-d');
                return Carbon::parse($item['timestamp'])->format('Y-m-d') >= Carbon::parse($timestamp)->format('Y-m-d');
            }
        )->all();

        return $filterAttendances;
    }

    /**
     * Save user
     */
    protected function saveUser(int $userId, $user) : void
    {
        $email = $userId . '@bpoc.co.jp';
        $userArray = [
            'user_id' => $userId,
            'email' => $email,
            'password' => 'hipe1108',
            'temporary_name' => $user['name']
        ];

        $this->userService->save($userArray);
    }

    /**
     * Get the first time in and last time out
     */
    protected function getTimeInsAndTimeOuts(int $userId, array $filterAttendances) : Collection
    {
        $timeInsAndTimeOuts = collect();

        foreach ($filterAttendances as $attendance) {
            $timestamp = $attendance['timestamp'];
            $timeInsAndTimeOuts[Carbon::parse($timestamp)->format('Y-m-d')] = Collection::make($filterAttendances)->filter(
                function ($item) use ($userId, $timestamp) {
                    return Carbon::parse($item['timestamp'])->format('Y-m-d') === Carbon::parse($timestamp)->format('Y-m-d') &&
                            $item['id'] == $userId;
                }
            );
        }

        return $timeInsAndTimeOuts;
    }

    /**
     * Save user attendance
     */
    private function saveUserAttendance(int $userId, Collection $timeInsAndTimeOuts): void
    {
        if ($timeInsAndTimeOuts->count() > 0) {
            foreach ($timeInsAndTimeOuts as $key => $timeInsAndTimeOut) {
                if ($timeInsAndTimeOut->count() > 0) {
                    $attendanceArray = [
                        'user_id' => (int)$userId,
                        'date' => Carbon::parse($timeInsAndTimeOut->first()['timestamp'])->format('Y-m-d'),
                        'time_in' => $timeInsAndTimeOut->first()['timestamp'],
                        'time_out' => $timeInsAndTimeOut->last()['timestamp'],
                        'state' => $timeInsAndTimeOut->first()['state'],
                        'location' => Locations::OFFICE
                    ];
    
                    $this->userAttendanceService
                        ->save($userId, $attendanceArray);
                }
            }
        }
    }
}
