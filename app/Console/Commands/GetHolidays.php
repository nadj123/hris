<?php

namespace App\Console\Commands;

use App\Libraries\GoogleCalendarLib;
use App\Models\Holiday;
use Illuminate\Console\Command;

class GetHolidays extends Command
{
    CONST JAPANESE_HOLDAY = 'japanese';
    CONST PHILIPPINE_HOLDAY = 'philippines';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:holidays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get holidays of PH and JA using google calendar';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $calendar = new GoogleCalendarLib();
        $phHolidays = $calendar->getHolidays(self::PHILIPPINE_HOLDAY);
        $jaHolidays = $calendar->getHolidays(self::JAPANESE_HOLDAY);

        $this->saveHolidays($phHolidays);
        $this->saveHolidays($jaHolidays);

        return Command::SUCCESS;
    }

    /**
     * Save holidays
     */
    public function saveHolidays(array $holidays)
    {
        foreach ($holidays as $holiday) {
            $requests = [
                'country' => substr($holiday['country'], 0, 2),
                'title' => $holiday['summary'],
                'description' => $holiday['description'],
                'start_date' => $holiday['start'],
                'end_date' => $holiday['end'],
            ];
            
            Holiday::updateOrCreate($requests, $requests);
        }
    }
}
