<?php
namespace App\Services;

use Illuminate\Contracts\Pagination\Paginator;
use App\Models\UserLeave;
use App\Repositories\Interfaces\IUserLeaveRepository;
use Illuminate\Database\Eloquent\Collection;

class UserLeaveService 
{
    public $userLeaveRepository;
    
    /**
     * User leave service constructor
     */
    public function __construct(IUserLeaveRepository $iUserLeaveRepository)
    {
        $this->userLeaveRepository = $iUserLeaveRepository;
    }

    /**
     * Get list of user leaves with pagination
     */
    public function getLeavesWithPagination() : Paginator
    {
        $columns = ['*'];
        $page = request()['page'] ?? 1;
        $limit = request()['limit'] ?? 20;

        return $this->userLeaveRepository
            ->model
            ->with(['user'])
            ->limit($limit)
            ->paginate(
                $limit,
                $columns,
                'page',
                $page
            );
    }

    /**
     * Get list of user with attendance and date pagination
     */
    public function getLeavesWithFilter(array $filters) : Collection
    {
        return $this->userLeaveRepository
            ->model
            ->with(['user.information.department', 'user.information.branch'])
            ->where(function($query) use ($filters) {
                if ($filters['start_date'] && $filters['end_date']) {
                    $query->whereBetween('start_date', [
                        $filters['start_date'], 
                        $filters['end_date']
                    ]);
                }
            })
            // ->whereHas('user.information.branch', function ($query) use ($filters) {
            //     if ($filters['branch_id']) {
            //         $query->where('branch_id', $filters['branch_id']);
            //     }
            // })
            // ->whereHas('user.information.department', function ($query) use ($filters) {
            //     if ($filters['department_id']) {
            //         $query->where('department_id', $filters['department_id']);
            //     }
            // })
            ->get();
    }

    /**
     * Save user leave.
     */
    public function save(int $userId, array $leave, int $userLeaveId = null) : UserLeave
    {
        $conditionData = [
            'id' => $userLeaveId
        ];

        $data = [
            'user_id' => $userId,
            'start_date' => $leave['start_date'],
            'end_date' => $leave['half_day'] ? $leave['start_date'] : $leave['end_date'],
            'reason' => $leave['reason'],
            'initial_approver' => $leave['initial_approver'],
            'type' => $leave['type'],
            'half_day' => $leave['half_day'],
            'post_meridiem' => $leave['post_meridiem']
        ];
        
        return $this->userLeaveRepository
            ->model
            ->updateOrCreate($conditionData, $data)
            ->load(['user.information.department']);
    }

    /**
     * Change leave status of user leave
     */
    public function changeStatus(int $authUserId, int $leaveId, int $status) : UserLeave
    {
        $leave =  $this->userLeaveRepository->findById($leaveId);
        $leave->status = $status;
        $leave->approver_id = $authUserId;
        $leave->save();

        return $leave->load(['user.information.department']);
    }

    /**
     * Delete leave by Id
     */
    public function deleteLeave(int $leaveId): bool
    {
        return $this->userLeaveRepository->deleteById($leaveId);
    }
}