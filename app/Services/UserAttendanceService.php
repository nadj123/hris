<?php
namespace App\Services;

use App\Constants\Locations;
use App\Repositories\Interfaces\IUserRepository;
use Illuminate\Contracts\Pagination\Paginator;
use App\Models\UserAttendance;
use App\Repositories\Interfaces\IUserAttendanceRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class UserAttendanceService 
{
    public $userRepository;
    public $userAttendanceRepository;
    
    /**
     * User attendance service constructor
     */
    public function __construct(
        IUserRepository $iUserRepository,
        IUserAttendanceRepository $iUserAttendanceRepository
    )
    {
        $this->userRepository = $iUserRepository;
        $this->userAttendanceRepository = $iUserAttendanceRepository;
    }

    /**
     * Get list of user attendance with pagination
     */
    public function getAttendances() : Paginator
    {
        $columns = ['*'];
        $page = request()['page'] ?? 1;
        $limit = request()['limit'] ?? 20;

        return $this->userAttendanceRepository
            ->model
            ->with(['user'])
            ->limit($limit)
            ->paginate(
                $limit,
                $columns,
                'page',
                $page
            );
    }

    /**
     * Get list of user with attendance and date pagination
     */
    public function getAttendancesByDate(array $filters) : Collection
    {
        return $this->userRepository
            ->model
            ->with(['information', 'attendance' => function ($query) use ($filters) {
                $query->where(function($query) use ($filters) {
                    if ($filters['start_date'] && $filters['end_date']) {
                        $query->whereBetween('date', [
                            $filters['start_date'], 
                            $filters['end_date']
                        ]);
                    }
                });
            }])
            ->where(function ($query) use ($filters) {
                if ($filters['branch_id']) {
                    $query->whereHas('information.branch', function ($query) use ($filters) {
                        $query->where('branch_id', $filters['branch_id']);
                    });
                }

                if ($filters['department_id']) {
                    $query ->whereHas('information.department', function ($query) use ($filters) {
                        $query->where('department_id', $filters['department_id']);
                    });
                }
            })
            ->get();
    }

    /**
     * Save user attendance.
     */
    public function save(int $userId, array $attendance) : UserAttendance
    {
        $conditionData = [
            'id' => $attendance['id'] ?? null,
            'user_id' => $userId,
            'date' => $attendance['date'],
        ];

        $data = [
            'user_id' => $userId,
            'date' => $attendance['date'],
            'time_in' => $attendance['time_in'],
            'time_out' => $attendance['time_out'] ?? null,
            'state' => $attendance['state'] ?? null,
            'location' => $attendance['location']
        ];
        
        return $this->userAttendanceRepository
            ->model
            ->firstOrCreate($conditionData, $data);
    }

    /** 
     * Get attendance by ID
     */
    public function getAttendanceLogByID(int $attendanceId)
    {
        return $this->userAttendanceRepository->findById($attendanceId);
    }

    /**
     * Save time in of the user.
     */
    public function timeInWFH(int $userId) : UserAttendance
    {
        $attendanceRequest = [
            'user_id' => $userId,
            'date' => Carbon::now(),
            'time_in' => Carbon::now(),
            'location' => Locations::WFH
        ];

        return $this->userAttendanceRepository->create($attendanceRequest);
    }

    /**
     * Save time out of the user based on today's date and time in of the user.
     */
    public function timeOutWFH(int $userId): UserAttendance
    {
        $todayAttendance = $this->getTodayAttendance($userId);

        $attendanceRequest = [
            'user_id' => $userId,
            'date' => Carbon::now(),
            'time_out' => Carbon::now(),
            'location' => Locations::WFH
        ];

        return $this->userAttendanceRepository->updateOrCreate(
            ['id' => $todayAttendance->id ?? null],
            $attendanceRequest
        );
    }

    /**
     * Get the auth user's attendance 
     * record for today.
     */
    public function getTodayAttendance(int $userId): ?UserAttendance
    {
        return $this->userAttendanceRepository->model
            ->where('date', Carbon::today())
            ->where('user_id', $userId)
            ->first();
    }
}