<?php
namespace App\Services;

use App\Constants\Roles;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Interfaces\IUserRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Role;

class UserService 
{
    const MAX_ATTEMPTS = 10;
    const SECONDS_LOCKED = 3600;
    const ONE_HOUR = 1;

    CONST PASSWORD_GRANT_CLIENT_ID = 2;
    CONST GRANT_TYPE = 'password';

    public $userRepository;
    protected $client;

    /**
     * User service constructor
     */
    public function __construct(IUserRepository $iUserRepository)
    {
        $this->userRepository = $iUserRepository;
        $this->client = DB::table('oauth_clients')->where('id', self::PASSWORD_GRANT_CLIENT_ID)->first();
    }

    /**
     * Create user.
     * 
     * @param string $email
     * 
     * @return \App\Models\User
     */
    public function findByEmail(string $email)
    {
        if ($email) {
            return $this->userRepository
                ->findByEmail($email);
        }
        
        return false;
    }

    /**
     * Update or create user.
     */
    public function save(array $userInformation) : User
    {
        $conditionData = [
            'id' => $userInformation['user_id'] ?? null
        ];

        $data = [
            'temporary_name' => $userInformation['temporary_name'] ?? null,
            'email' => $userInformation['email'],
            'password' => Hash::make($userInformation['password'])
        ];

        $user = $this
            ->userRepository
            ->model
            ->firstOrCreate($conditionData, $data);

        $user->assignRole($userInformation['role_id'] ?? Roles::USER_ROLE);
        return $user;
    }

    /**
     * Create user.
     */
    public function updatePassword(int $userId, string $password) : User
    {
        $data = [
            'password' => Hash::make($password)
        ];

        return $this->userRepository
            ->update($userId, $data);
    }

    /**
     * Request oauth token.
     * 
     * @param $loginRequest
     */
    public function getOauthToken($loginRequest)
    {
        request()->request->add([
            'username' => $loginRequest->email,
            'password' => $loginRequest->password,
            'grant_type' => self::GRANT_TYPE,
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'scope' => ''
        ]);
        
        $proxy = request()->create(
            'oauth/token',
            'POST'
        );
        $token = Route::dispatch($proxy);
        return json_decode($token->getContent(), true);
    }

    /**
     * Get the rate limiting throttle key for the request.
     */
    public function throttleKey() : string
    {
        return str()->lower(request('email')) . '|' . request()->ip();
    }

    /**
     * Ensure the login request is not rate limited.
     */
    public function isTooManyFailedAttempts()
    {
        return RateLimiter::tooManyAttempts($this->throttleKey(), self::MAX_ATTEMPTS) ? true : false;
    }

    /**
     * Check if password reset token is still valid or not.
     */
    public function isResetTokenValid(string $email, string $token) : bool
    {
        $passwordResetData = PasswordReset::where('email', $email)
            ->where('created_at', '>', Carbon::now()->subHours(self::ONE_HOUR))
            ->first();

        if ($token && Hash::check($token, $passwordResetData->token)) {
            return true;
        }
        return false;
    }

    /**
     * Check if user role is with the 
     * approvers scope
     */
    public function isApprover(User $user) : bool
    {
        return $user->hasRole('super-admin') || 
               $user->hasRole('admin') ||
               $user->hasRole('manager') ||
               $user->hasRole('team-lead');
    }
}
