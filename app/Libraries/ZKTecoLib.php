<?php

namespace App\Libraries;

use Rats\Zkteco\Lib\ZKTeco;

class ZKTecoLib
{
    CONST IP_ADDRESS = '192.168.0.125';
    CONST PORT = 4370;

    public $zkTeco;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->zkTeco = new ZKTeco(self::IP_ADDRESS, 4370);
        $this->zkTeco->connect();
    }

    /**
     * Connect to device
     */
    public function connect()
    {
        return $this->zkTeco->connect();
    }

    /**
     * Connect to device
     */
    public function disconnect()
    {
        return $this->zkTeco->disconnect();
    }

    /**
     * Get attendance all employee attendance
     */
    public function getUsers()
    {
        $users = $this->zkTeco->getUser();
        return $users;
    }

    /**
     * Get attendance all employee attendance
     */
    public function getAttendances()
    {
        $attendances = $this->zkTeco->getAttendance();
        return $attendances;
    }
}
