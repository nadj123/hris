<?php

namespace App\Libraries;

use Illuminate\Support\Facades\Log;

class GoogleCalendarLib
{
    protected $client;

    public function __construct()
    {
        $this->client = new \Google\Client();
        $this->client->setDeveloperKey(env('GOOGLE_API_KEY'));
        $this->client->setAuthConfig(config('services.google_calendar.credentials_path'));
        $this->client->setScopes(['https://www.googleapis.com/auth/calendar.readonly']);
    }

    public function getHolidays(string $country): array
    {
        $service = new \Google\Service\Calendar($this->client);
        $calendarId = "en.$country#holiday@group.v.calendar.google.com";

        try {
            $events = $service->events->listEvents($calendarId);

            $holidays = [];
            foreach ($events->getItems() as $event) {
                $holidays[] = [
                    'country' => $country,
                    'summary' => $event->getSummary(),
                    'description' => $event->getDescription(),
                    'start' => $event->getStart()->date,
                    'end' => $event->getEnd()->date,
                ];
            }

            return $holidays;
        } catch (\Google\Exception $e) {
            // Handle the API error
            // You can log the error or return an empty array, depending on your needs
            Log::error($e->getMessage());
            return [];
        }
    }
}
