<?php

namespace App\Rules;

use App\Services\UserAttendanceService;
use Illuminate\Contracts\Validation\Rule;

class AlreadyTimedIn implements Rule
{
    protected $userAttendanceService;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(UserAttendanceService $userAttendanceService)
    {
        $this->userAttendanceService = $userAttendanceService;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $userId = auth()->user()->id;
        $todayAttendance = $this->userAttendanceService
            ->getTodayAttendance($userId);

        $timeIn = $todayAttendance['time_in'] ?? null;

        return $timeIn ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "The user has already timed in, don't make any modification!";
    }
}
