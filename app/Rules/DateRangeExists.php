<?php

namespace App\Rules;

use App\Models\UserLeave;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class DateRangeExists implements Rule
{
    protected $table;
    protected $columnStart;
    protected $columnEnd;
    protected $columnUserId;

    /**
     * Create a new rule instance.
     */
    public function __construct(
        string $table, 
        string $columnStart, 
        string $columnEnd, 
        string $columnUserId = null
    )
    {
        $this->table = $table;
        $this->columnStart = $columnStart;
        $this->columnEnd = $columnEnd;
        $this->columnUserId = $columnUserId;
    }

    /**
     * Determine if the validation rule passes.
     */
    public function passes(mixed $attribute, mixed $value): bool
    {
        $leaveId = request()['id'] ?? null;

        $exists = UserLeave::where(function ($query) {
                $query->whereBetween($this->columnStart, [
                    request()[$this->columnStart], 
                    request()[$this->columnEnd]
                ])
                ->orWhereBetween($this->columnEnd, [
                    request()[$this->columnStart], 
                    request()[$this->columnEnd]
                ]);
            })
            ->where(function ($query) {
                if ($this->columnUserId) {
                    $query->where($this->columnUserId, auth()->user()->id);
                }
            })
            ->exists();

        return !$leaveId ? !$exists : true;
    }

    /**
     * Get the validation error message.
     */
    public function message(): string
    {
        return 'The :attribute already exists.';
    }
}
