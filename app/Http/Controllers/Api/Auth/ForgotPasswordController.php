<?php
namespace App\Http\Controllers\Api\Auth;

use Illuminate\Support\Str;
use App\Services\UserService;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Constants\Declare\HttpCode;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use App\Constants\Declare\HttpStatus;
use App\Events\ResetPasswordConfirmed;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use App\Http\Requests\Api\Auth\ResetPasswordRequest;
use App\Http\Requests\Api\Auth\ForgotPasswordRequest;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\RateLimiter;

class ForgotPasswordController extends Controller
{
    CONST INVALID_TOKEN = Password::INVALID_TOKEN;
    CONST PASSWORD_RESET = Password::PASSWORD_RESET;
    CONST FORGOT_PASSWORD_PATH = '/forgot/password/reset';

    public $userService;

    /**
     * AuthController constructor
     * 
     * @param UserService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Get reset link token.
     * 
     * TODO: Need maayos na message response at hindi
     * dapat naka static at imbose dito sa controller.
     *
     * @param \Http\Requests\Api\Auth\ForgotPasswordRequest
     * @param bool $platform
     * 
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function tokenResetLink(ForgotPasswordRequest $request, bool $mobile = true)
    {
        $email = $request['email']; 
        $token = $request['token'];
 
        if ($this->userService->isResetTokenValid($email, $token) == true) {
            $frontEndUrl = env('APP_FRONT_END_URL');
            $path = self::FORGOT_PASSWORD_PATH;

            if (!$mobile) {
                $concatenatedLink = $frontEndUrl.$path.'?token='.$token.'&email='.$email;
                return redirect()->away($concatenatedLink);
            }

            event(new ResetPasswordConfirmed($token, $email));
            return 'Go back to the app';
        }

        return 'Your password reset link is already expired';
    }

    /**
     * Forgot password of the user.
     * 
     * @param \Http\Requests\Api\Auth\ForgotPasswordRequest
     * @return \Flugg\Responder\Responder
     */
    public function forgot(ForgotPasswordRequest $request) 
    {
        DB::beginTransaction();
        try {
            Password::sendResetLink($request->only('email'));

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            return responder()
                ->error(HttpCode::SERVER_ERROR, $e->getMessage());
        }

        return responder()->success([
            'message' => trans('passwords.sent')
        ]);
    }

    /**
     * Reset password using em,ail & token.
     * 
     * @param  \Http\Requests\Api\ResetPasswordRequest
     * @return \Flugg\Responder\Responder
     */
    public function reset(ResetPasswordRequest $request) 
    {
        DB::beginTransaction();
        try {
            $reset = Password::reset(
                $request->only('email', 'password', 'password_confirmation', 'token'),
                function ($user, $password) {
                    $user->forceFill([
                        'password' => Hash::make($password)
                    ])->setRememberToken(Str::random(60));
                    $user->save();
    
                    event(new PasswordReset($user));
                    event(new Verified($user));
                    RateLimiter::clear($this->userService->throttleKey());
                }
            );

            if ($reset === self::INVALID_TOKEN) {
                return responder()
                    ->error(HttpCode::INVALID_TOKEN)
                    ->respond(HttpStatus::UNAUTHORIZED);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());
            return responder()
                ->error(HttpCode::SERVER_ERROR);
        }

        return responder()->success([
            'message' => trans('passwords.reset')
        ]);
    }
}
