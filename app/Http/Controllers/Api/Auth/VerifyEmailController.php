<?php

namespace App\Http\Controllers\Api\Auth;

use App\Constants\Declare\HttpCode;
use App\Constants\Declare\HttpStatus;
use App\Events\EmailVerificationConfirmed;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\User;
use Exception;

class VerifyEmailController extends Controller
{
    public function verify(Request $request): string
    {
        $user = User::find($request->route('id'));

        if (!$user) {
            return 'User not found.';
        }

        if ($user->hasVerifiedEmail()) {
            return 'Your email address is already verified. Go back to app';
        }

        if ($user->isTokenExpired()) {
            return 'Your email verification is already expired. Please resend a new email verification using the app!';
        }

        if ($user->markEmailAsVerified()) {
            $email = $user->email;
            event(new EmailVerificationConfirmed($email));
            event(new Verified($user));

            return 'Your email address has been verified. Go back to app';
        }

        return 'Something went wrong during email verification.';
    }

    public function resend(Request $request)
    {
        try {
            $user = auth()->user();

            if (!$user) {
                return responder()
                    ->error("User not found. Register again!")
                    ->respond(HttpStatus::UNAUTHORIZED);
            }

            if ($user->hasVerifiedEmail()) {
                return responder()
                    ->success(["message" => "Email already verified."])
                    ->respond(HttpStatus::UNAUTHORIZED);
            }

            $request->user()->sendEmailVerificationNotification();
            return responder()
                ->success([
                    'message' => 'Successfully sent the verification through your email.'
                ]);
        } catch (Exception $e) {
            return responder()
                ->error($e->getMessage())
                ->respond();
        }
    }
}
