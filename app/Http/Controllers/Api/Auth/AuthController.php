<?php

namespace App\Http\Controllers\Api\Auth;

use App\Constants\Declare\HttpCode;
use App\Constants\Declare\HttpStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Models\User;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\RateLimiter;

class AuthController extends Controller
{
    public $userService;
    public $client;
    
    CONST PASSWORD_GRANT_CLIENT_ID = 2;
    CONST GRANT_TYPE = 'password';

    /**
     * AuthController constructor
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Get the authenticated User.
     */
    public function me() : JsonResponse
    {
        /** @var User $user */
        $user = auth()->user();
        $authUser = $user->load([
            'roles.permissions', 
            'information.department', 
            'information.position',
            'information.branch',
        ]);

        return responder()->success($authUser)->respond();
    }

    /**
     * Authenticate user.
     */
    public function login(LoginRequest $request) : JsonResponse
    {
        try {
            $isTooManyFailedAttempts = $this->userService
                ->isTooManyFailedAttempts();
                
            if (
                !auth()->attempt($request->only('email', 'password')) || 
                $isTooManyFailedAttempts
            ) {
                RateLimiter::hit(
                    $this->userService->throttleKey(), 
                    $this->userService::SECONDS_LOCKED
                );

                $code = $isTooManyFailedAttempts ? 
                    HttpCode::EXCEED_LOGIN_ATTEMPTS : HttpCode::INVALID_LOGIN;

                return responder()
                    ->error($code)
                    ->respond(HttpStatus::MISDIRECTED_REQUEST);
            }

            /** @var User $user */
            $user = auth()->user();
            $token = $user->createToken('nanosoft_attendance')->accessToken;

            RateLimiter::clear($this->userService->throttleKey());
            return responder()->success(['token' => $token])->respond();
                
        } catch (Exception $e) {
            return responder()
                ->error(HttpCode::SERVER_ERROR, $e->getMessage())
                ->respond(HttpStatus::SERVER_ERROR);
        }
    }

    /**
     * Get the authenticated User.
     */
    public function logout() : JsonResponse
    {
        /** @var User $user */
        $user = auth()->user();
        $user->token()->revoke();

        return responder()->success([
            'message' => trans('auth.logout')
        ])->respond();
    }
}
