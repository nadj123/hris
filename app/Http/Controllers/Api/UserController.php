<?php

namespace App\Http\Controllers\Api;

use App\Traits\HandlesTransactionTrait;
use App\Constants\Declare\HttpStatus;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\UserRequest;
use App\Services\UserInformationService;

class UserController extends Controller
{
    use HandlesTransactionTrait;

    public $userService;
    public $userInformationService;

    /**
     * Constructor
     */
    public function __construct(
        UserService $userService,
        UserInformationService $userInformationService
    )
    {
        $this->userService = $userService;
        $this->userInformationService = $userInformationService;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function registration(UserRequest $request) : JsonResponse
    {
        return $this->runInTransaction(function () use ($request) {
            $requests = $request->all();
            
            $createdUser = $this->userService->save($requests);
            $this->userInformationService->save($createdUser->id, $requests);
            $token = $createdUser->createToken('HRIS')->accessToken;
            
            return responder()->success(['token' => $token])
                ->respond(HttpStatus::CREATED);
        });
    }
}
