<?php

namespace App\Http\Controllers\Api;

use App\Traits\HandlesTransactionTrait;
use App\Constants\LeaveStatuses;
use App\Events\LeaveUpdated;
use App\Http\Controllers\Controller;
use App\Services\UserLeaveService;
use Illuminate\Http\JsonResponse;

class AdminLeaveController extends Controller
{
    use HandlesTransactionTrait;

    public $userLeaveService;

    /**
     * Constructor
     */
    public function __construct(UserLeaveService $userLeaveService)
    {
        $this->userLeaveService = $userLeaveService;
    }

    /**
     * Approve user leave request
     */
    public function approveLeave(int $leaveId) : JsonResponse
    {
        return $this->runInTransaction(function () use ($leaveId) {
            $approvedLeaved = $this
                ->userLeaveService
                ->changeStatus(
                    auth()->user()->id,
                    $leaveId, 
                    LeaveStatuses::APPROVED
                );

            event(new LeaveUpdated($approvedLeaved));
            
            return responder()
                ->success($approvedLeaved)
                ->respond();
        });
    }

    /**
     * Decline user leave request
     */
    public function declineLeave(int $leaveId) : JsonResponse
    {
        return $this->runInTransaction(function () use ($leaveId) {
            $declinedLeave = $this
                ->userLeaveService
                ->changeStatus(
                    auth()->user()->id,
                    $leaveId, 
                    LeaveStatuses::DECLINED
                );

            event(new LeaveUpdated($declinedLeave));
            
            return responder()
                ->success($declinedLeave)
                ->respond();
        });
    }
}
