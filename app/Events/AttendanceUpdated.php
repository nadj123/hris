<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AttendanceUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userAttendance;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $userAttendance)
    {
        $this->userAttendance = $userAttendance;
    }

    /**
     * The event's broadcast name.
    *
    * @return string
    */
    public function broadcastAs()
    {
        return 'attendance-updated-event';
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith() 
    {
        return [
            'userAttendance' => $this->userAttendance,
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('attendance-updated');
    }
}
