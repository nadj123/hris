<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EmailVerificationConfirmed implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $email;

    /**
     * Create a new event instance.
     *
     * @param string $email 
     * @return void
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * The event's broadcast name.
    *
    * @return string
    */
    public function broadcastAs()
    {
        return 'email-verification-confirmed-event';
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith() 
    {
        return [
            'email' => $this->email
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('email-verification-confirmed.' . $this->email);
    }
}
