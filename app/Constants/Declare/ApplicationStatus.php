<?php
namespace App\Constants\Declare;

class ApplicationStatus
{
    CONST NO_ACTION = 0;
    CONST PENDING = 1;
    CONST APPROVED = 2;
    CONST REJECTED = 3;
}