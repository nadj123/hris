<?php
namespace App\Constants\Declare;

class HttpCode
{
    CONST OK = 'ok';
    CONST CREATED = 'created';
    CONST UNAUTHORIZED = 'unauthorized';
    CONST SERVER_ERROR = 'server_error';
    CONST INVALID_LOGIN = 'invalid_login';
    CONST INVALID_TOKEN = 'invalid_token';
    CONST VALIDATION_FAILED = 'validation_failed';
    CONST EXCEED_LOGIN_ATTEMPTS = 'exceed_login_attempts';
    CONST EMAIL_NOT_VERIFIED = 'email_not_verified';
}