<?php

namespace App\Models;

use App\Constants\LeaveStatuses;
use App\Constants\LeaveTypes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLeave extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are not mass assignable.
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['type_desc', 'status_desc'];

    /**
     * Get user associated with the user leave.
     */
    public function user()
    {
        return $this->belongsTo(User::class)
            ->with(['information']);
    }

    /**
     * Get the description of the leave type.
     */
    protected function typeDesc(): Attribute
    {
        return Attribute::make(
            get: fn () => LeaveTypes::toHuman($this->type),
        );
    }

    /**
     * Get the description of the leave type.
     */
    protected function statusDesc(): Attribute
    {
        return Attribute::make(
            get: fn () => LeaveStatuses::toHuman($this->status),
        );
    }
}
