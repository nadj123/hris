<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserInformation extends Model
{
    use HasFactory;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The primary key of the model
     * 
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['full_name'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'birth_date',
        'hired_date',
        'resigned_date'
    ];

    /**
     * Timestamps for updated_at and created_at.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get full name of the student
     */
    public function getFullNameAttribute() : string
    {
        $fullNameArray = [
            $this->first_name,
            $this->last_name
        ];

        return implode(' ', $fullNameArray);
    }

    /**
     * Get the addresses associated to the user information.
     */
    public function addresses()
    {
        return $this->hasMany(UserAddress::class);
    }

    /**
     * Get the emergency contacts associated to the user information.
     */
    public function emergencyContacts()
    {
        return $this->hasMany(UserEmergencyContact::class);
    }

    /**
     * Get the department associated to the user information.
     */
    public function branch()
    {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }

    /**
     * Get the department associated to the user information.
     */
    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    /**
     * Get the position associated to the user information.
     */
    public function position()
    {
        return $this->hasOne(Position::class, 'id', 'position_id');
    }

    /**
     * Eager loads everything with the user information.
     */
    public function withAllInformation()
    {
        return $this->load(['addresses', 'emergencyContacts', 'department', 'position']);
    }
}
