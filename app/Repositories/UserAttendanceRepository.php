<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserAttendance;
use App\Repositories\Interfaces\IUserAttendanceRepository;

class UserAttendanceRepository extends BaseRepository implements IUserAttendanceRepository
{
    public $model;

    /**
     * constructor
     * 
     * @param Model $model
     */
    public function __construct(UserAttendance $model)
    {
        $this->model = $model;
    }
}