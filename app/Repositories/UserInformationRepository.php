<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserInformation;
use App\Repositories\Interfaces\IUserInformationRepository;

class UserInformationRepository extends BaseRepository implements IUserInformationRepository
{
    public $model;

    /**
     * UserRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(UserInformation $model)
    {
        $this->model = $model;
    }
}