<?php
namespace App\Repositories\Interfaces;

use App\Repositories\Interfaces\IBaseRepository;

interface IUserInformationRepository extends IBaseRepository {}