<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserLeave;
use App\Repositories\Interfaces\IUserLeaveRepository;

class UserLeaveRepository extends BaseRepository implements IUserLeaveRepository
{
    public $model;

    /**
     * UserRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(UserLeave $model)
    {
        $this->model = $model;
    }
}