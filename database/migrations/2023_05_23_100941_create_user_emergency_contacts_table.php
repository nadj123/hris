<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_emergency_contacts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('user_emergency_contact_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('name');
            $table->tinyInteger('relationship');
            $table->integer('mobile_number');
            $table->string('landine');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_emergency_contacts');
    }
};
