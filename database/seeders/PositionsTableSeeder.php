<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $roles = [
            'Web Developer',
            'Designer',
            'Admin Staff',
            'Software Engineer',
            'Front-end Engineer',
            'BPO Staff',
            'Game Developer',
            'Company Driver'
        ];

        foreach ($roles as $key => $value) {
            Position::create([
                'name' => $value
            ]);
        }
    }
}
