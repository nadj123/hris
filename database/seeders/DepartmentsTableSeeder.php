<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $roles = [
            [
                'name' => 'None',
                'hex_code' => '#B33771'
            ],
            [
               'name' => 'Mark',
               'hex_code' => '#1abc9c'
            ],
            [
               'name' => 'Bukken-Oh',
               'hex_code' => '#2ecc71'
            ],
            [
                'name' => 'Shin-san',
                'hex_code' => '#e67e22'
            ],
            [
                'name' => 'Lacicu',
                'hex_code' => '#34495e'
            ],
            [
                'name' => 'Walklog',
                'hex_code' => '#192a56'
            ],
            [
                'name' => 'Madori',
                'hex_code' => '#006266'
            ],
            [
                'name' => 'Oyster',
                'hex_code' => '#9c88ff'
            ],
            [
                'name' => 'Umed',
                'hex_code' => '#4a69bd'
            ],
            [
                'name' => 'Ruby',
                'hex_code' => '#c23616'
            ],
            [
                'name' => 'Teconet',
                'hex_code' => '#B33771'
            ],
            [
                'name' => 'HR/Admin',
                'hex_code' => '#D48C95'
            ],
        ];

        foreach ($roles as $key => $value) {
            Department::create([
                'name' => $value['name'],
                'hex_code' => $value['hex_code']
            ]);
        }
    }
}
